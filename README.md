# Sustainable Authorship and the quest for the perfect workflow

[![pipeline status](https://gitlab.com/martisak/sustainable-authorship/badges/master/pipeline.svg)](https://gitlab.com/martisak/sustainable-authorship/commits/master)

To strengthen and validate our scientific claims, _replication_ by a completely independent study is important --- but this can be time-consuming and costly and is hard in practice due to lack of modern tools. The ongoing trend in computer science with larger datasets, and more computation makes replication much more difficult. Students as well as experienced researchers leave an "electronic battlefield", which create nightmares for students and colleagues trying to build on previous work. We deliver a unifying framework, built on top of modern tools, that separates code, data and execution environment. Our work can be extended to work with any type of research output, making sure researchers survive the battle and live to fight another day.

You can [download the latest compiled paper here.](https://gitlab.com/martisak/sustainable-authorship/-/jobs/artifacts/master/download?job=pdf)

![First page of paper](./images/sustainable-authorship.png)
 
## Acknowledgment

This work is done as part of the [The Art of Doctoral Research (IL3606, PhD course, 7.5 pt) ](https://people.kth.se/~dubrova/coursePhD.html) course.

This work was partially supported by the Wallenberg AI, Autonomous Systems and Software Program (WASP) funded by the Knut and Alice Wallenberg Foundation.
