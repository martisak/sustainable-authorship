SOURCES=$(wildcard *.pmd)
# SOURCES=$(subst README.md,,${_SOURCES})

DIR=/usr/local

TEMPLATE=$(DIR)/article/article.template
TEMPLATE=$(DIR)/pandoc_conference_templates/NeurIPS_2018/neurips_2018.template
TEMPLATE=$(DIR)/pandoc_conference_templates/IEEEtran/ieee_conf.template

PDF_ENGINE=pdflatex

PANDOC_OPTIONS=--table-of-contents \
	--listings \
	--natbib \
	-M biblio-style="IEEEtranN" \
	-M link-citations=true \
	-M natbiboptions="numbers, sort" \
 	--top-level-division=section

# --highlight-style pygments \
# -M fignos-plus-name=Figure \
# -M fignos-cleveref=True \


PANDOC_FILTERS=-F pandoc-fignos

#--number-sections
PANDOC_REFERENCES=--bibliography=references/Other.bib

LATEXMK=latexmk
LATEXMK_OPTIONS=-bibtex -pdf -pdflatex="pdflatex -interaction=nonstopmode"

RM = /bin/rm -f
PANDOC=pandoc
PYTHON=python
RSCRIPT=Rscript
PARENTDIR = $(dirname `pwd`)
RMD_OBJECTS=$(SOURCES:.pmd=.Rmd)
MD_OBJECTS=$(SOURCES:.pmd=.md)
PDF_OBJECTS=$(SOURCES:.pmd=.pdf)
PNG_OBJECTS=$(SOURCES:.pmd=.png)
TEX_OBJECTS=$(SOURCES:.pmd=.tex)

DOCKER=docker
DOCKER_IMAGE=martisak/reproducibleresearch
DOCKER_COMMAND=run --rm
DOCKER_MOUNT=-v`pwd`:/data \
		-v `pwd`/references/:/data/references

all: render latex

pdf: $(PDF_OBJECTS) $(TEX_OBJECTS)
tex: $(RMD_OBJECTS) $(MD_OBJECTS) $(TEX_OBJECTS)
png: $(PDF_OBJECTS) $(PNG_OBJECTS)

%.png: %.pdf
	convert $< \
		-thumbnail "1280x800>" -density 300 \
		-background white -alpha remove  $@
	mv $(subst .png,-0.png,$@) $@
	rm $(subst .png,-?.png,$@)


%.Rmd: %.pmd
	pweave --input-format=markdown \
		--format=pandoc \
		--figure-format=pdf \
		--output=$@ \
		$<

%.md: %.Rmd
	$(RSCRIPT) -e "library(knitr); knit('$<')"

%.pdf: %.tex
	echo $<
	$(LATEXMK) $(LATEXMK_OPTIONS) $<

%.tex: %.md
	echo $<
	$(PANDOC) \
		-f markdown+table_captions  \
		$(PANDOC_FILTERS) \
		--pdf-engine=$(PDF_ENGINE) \
		--template=$(TEMPLATE) \
		$(PANDOC_OPTIONS) \
		$(PANDOC_REFERENCES) -o $@ $<

clean:
	-rm $(PDF_OBJECTS) $(TEX_OBJECTS) $(RMD_OBJECTS) $(MD_OBJECTS)
	-rm -rf figures figure
	- latexmk -bibtex -C -f $(TEX_OBJECTS)

latex:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) $(DOCKER_IMAGE) \
		make -C /data tex

render:
	$(DOCKER) $(DOCKER_COMMAND) $(DOCKER_MOUNT) $(DOCKER_IMAGE) \
		make -C /data pdf

debug:
	$(DOCKER) $(DOCKER_COMMAND) -it $(DOCKER_MOUNT) $(DOCKER_IMAGE) \
		bash

submods:
	git submodule update --recursive --remote

.INTERMEDIATE: $(RMD_OBJECTS) $(MD_OBJECTS)
