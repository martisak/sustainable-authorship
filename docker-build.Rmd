```{r dot-build-document-docker, echo=F, engine = "dot", fig.cap = "Document build pipeline using Docker. We input the source document itself, the data and the bibliography and get a PDF file as output. The source document can be in a variety of formats. The rendering of the document is done inside the container.", cache=F, dev="pdf", width="50%"}
digraph pipeline {
    margin=0
    bgcolor="transparent"
    rankdir = LR;
    node [margin=0.1 fontsize=8 shape=box style=filled, width=1]
    node [color="#242424" fontname = "helvetica", fontcolor="#f2f2f2"]
    edge [color="#767676" margin=0.1 fontsize=8 fontname = "helvetica", fontcolor="#f2f2f2"]
    fontsize = 8
    fontname = "helvetica"
    fontcolor="#f2f2f2"
    labeljust = "l"
    splines=ortho

    {
        node [color="#f2c618" fontcolor="#242424"]
        pdf [label="PDF document"]
        latex [label="LaTeX\ndocument"]
    }

    {
        node [color="#f78521" fontcolor="#242424"]
        "Data"
        "Bright idea"
        "Bibliography"
        rank=same;
    }

    subgraph cluster_1 {
        node [color="#0082f0" fontcolor="#f2f2f2"]
        color="transparent"
        container [label="martisak/\nreproducibleresearch"]
    }

    subgraph cluster_2 {
        color="transparent"
        pdf
    }

    subgraph cluster_3 {
        color="transparent"
        source [label="Source\ndocument"]
    }

    "Data" -> source
    "Bright idea" -> source
    source -> container
    container -> pdf
    container -> latex
    "Bibliography" -> source [style="dashed"]

}
```